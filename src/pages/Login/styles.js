
import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({

    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

    welcomeStyles: {
        color: 'red',
        fontSize: 25,
        fontWeight: '800'
    },

    helloStyles: {
        color: 'green',
        fontSize: 22,
        fontWeight: '500'
    },

    input: {
        borderWidth: 2,
        borderColor: 'black',
        borderRadius: 15,
        height: 60,
        width: '80%',
        padding: 10,
        fontSize: 22,
        fontWeight: '900',
        marginVertical: 10
    },

    button: {
        width: '50%',
        backgroundColor: 'blue',
      borderRadius:20,
        padding: 15,
        alignItems:'center',
        justifyContent:'center',
    },

    btn_text : {
        color: '#fff',
        fontSize: 25,
    }


})