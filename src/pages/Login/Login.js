
import React, { useEffect, useState } from "react";
import { Text, View, StyleSheet, TextInput, TouchableOpacity } from "react-native";
import { styles } from './styles';

const Login = () => {


    const [name, setName] = useState('G Ravi Kumar');

    const [userName , setUserName] = useState('');
    const [ password, setPassword] = useState('');


    const loginClick = () =>{

        console.log("userName",userName);
        console.log("password",password);

    }


    return (
        <View style={styles.container}>

            {/* <Text style={styles.welcomeStyles}>Welcome Ravi</Text>

            <Text style={styles.helloStyles}>Hello Ravi</Text>

            <Text style={{
                color: 'blue',
                fontSize: 35,
                fontWeight: '800'
            }}>How are you {name}</Text>

            <View>
                <Text>I am Krishna</Text>
            </View> */}

            <TextInput
                placeholder="User Name"
                style={styles.input}
                value={userName}
                onChangeText={(text)=>{
                    setUserName(text)
                }}
            />

            <TextInput
                placeholder="Password"
                style={styles.input}
                value={password}
                onChangeText={(text)=>{
                    setPassword(text)
                }}
            />

            <TouchableOpacity style={styles.button} onPress={loginClick}>
               <Text style={styles.btn_text}> Login</Text>
            </TouchableOpacity>


        </View>
    )
}

export default Login;

